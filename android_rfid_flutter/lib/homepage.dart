import 'package:flutter/material.dart';

class HomePageWidget extends StatefulWidget{
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePageWidget>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: new ListView(
          children: <Widget>[
            _getCard('连接', Icons.cast_connected, Colors.lightBlueAccent),
            _getCard('断开', Icons.clear, Colors.redAccent),
            _getCard('启动', Icons.play_arrow, Colors.lightGreenAccent),
            _getCard('停止', Icons.pause, Colors.red),
            _getCard('清除', Icons.clear_all, Colors.purple),
            _getCard('提交', Icons.cloud_upload, Colors.blueAccent),
          ],
        ),
      ),
    );
  }
}

Widget _getCard (String text, IconData icon, Color color) {
  var checkBoxVale = false;
  return new Card(
    margin: const EdgeInsets.only(left: 20.0, right: 40.0, top: 10, bottom: 10),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: color,
    child: new ListTile(
      title: new Text(text),
      leading: new Icon(icon),
      trailing: new Checkbox(
        value: checkBoxVale,
        onChanged: null,
      ),
    ),
  );
}