import 'package:flutter/material.dart';

class SettingsPageWidget extends StatefulWidget {
  @override
  _SettingsPageState createState() => new _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPageWidget> {
  var check = false;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new SingleChildScrollView(
        child: new Column(
          children: <Widget>[
            TextField(
              autofocus: false,
              decoration: InputDecoration(
                labelText: "DHM服务器地址",
                prefixIcon: Icon(Icons.settings),
              ),
              obscureText: true,
            ),
            TextField(
              autofocus: false,
              decoration: InputDecoration(
                labelText: "设备MAC地址",
                prefixIcon: Icon(Icons.settings),
              ),
              obscureText: true,
            ),
            TextField(
              autofocus: false,
              decoration: InputDecoration(
                labelText: "初始默认功率",
                prefixIcon: Icon(Icons.settings),
              ),
              obscureText: true,
            ),
            TextField(
              autofocus: false,
              decoration: InputDecoration(
                labelText: "提交接口地址",
                prefixIcon: Icon(Icons.settings),
              ),
              obscureText: true,
            ),
            RaisedButton(
              child: new Text('确认'),
              onPressed: () {},
            ),
            CheckboxListTile(
              secondary: const Icon(Icons.enhanced_encryption),
              title: const Text('加密'),
              value: check,
              onChanged: (bool value) {
                setState(() {
                  check = !check;
                });
              },
            ),
            TextField(
              autofocus: false,
              decoration: InputDecoration(
                labelText: "原密码",
                prefixIcon: Icon(Icons.settings),
              ),
              obscureText: true,
            ),
            TextField(
              autofocus: false,
              decoration: InputDecoration(
                labelText: "新密码",
                prefixIcon: Icon(Icons.settings),
              ),
              obscureText: true,
            ),
            TextField(
              autofocus: false,
              decoration: InputDecoration(
                labelText: "重复新密码",
                prefixIcon: Icon(Icons.settings),
              ),
              obscureText: true,
            ),
            RaisedButton(
              child: new Text('确认'),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}