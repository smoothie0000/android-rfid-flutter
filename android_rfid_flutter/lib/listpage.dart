import 'package:flutter/material.dart';

class ListPageWidget extends StatefulWidget {
  @override
  _ListPageState createState() => new _ListPageState();
}

class _ListPageState extends State<ListPageWidget> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            child: DataTable(
              columns: <DataColumn>[
                DataColumn(
                  label: Text('序号'),
                ),
                DataColumn(
                  label: Text('EPC'),
                ),
                DataColumn(
                  label: Text('其他信息1'),
                ),
                DataColumn(
                  label: Text('其他信息2'),
                ),
              ],
              rows: <DataRow>[],
            ),
          ),
          Expanded(
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: RaisedButton(
                child: new Text('修改EPC'),
                onPressed: () {},
              ),
            )
          )
        ],
      )
    );
  }
}
