import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'bottomnavigator.dart';

void main() => runApp(MyApp());

ThemeData buildTheme() {
  final ThemeData base = ThemeData();

  return base.copyWith(
    primaryColor: Color(0XFFCCCCCC),
    indicatorColor: Color(0xFFCCCCCC),
    hintColor: Colors.black45,
    inputDecorationTheme: InputDecorationTheme(
      hintStyle: new TextStyle(color: Colors.red),
      labelStyle: TextStyle(
        color: Color(0XFFCCCCCC),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark
        .copyWith(statusBarIconBrightness: Brightness.dark));
    return new MaterialApp(
      title: 'MongoX RFID',
      debugShowCheckedModeBanner: true,
      theme: buildTheme(),
      home: new BottomNavigatorBarWidget(),
    );
  }
}
