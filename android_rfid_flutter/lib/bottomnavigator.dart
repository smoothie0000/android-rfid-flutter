import 'package:flutter/material.dart';
import 'settingspage.dart';
import 'homepage.dart';
import 'listpage.dart';

List<Widget> _pageOptions = <Widget>[
  new HomePageWidget(),
  new SettingsPageWidget(),
  new ListPageWidget(),
];

class BottomNavigatorBarWidget extends StatefulWidget {
  @override
  _BottomNavigatorBarState createState() => new _BottomNavigatorBarState();
}

class _BottomNavigatorBarState extends State<BottomNavigatorBarWidget> {
  int _bottomNavIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: new BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _bottomNavIndex,
        onTap: (int index){
          setState((){
            _bottomNavIndex = index;
          });
        },
        fixedColor: Color(0XFF29D091),
        items: [
          new BottomNavigationBarItem(
              icon: new Icon(Icons.home),
              title: new Text('')
          ),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.settings),
              title: new Text('')
          ),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.list),
              title: new Text('')
          ),
        ],
      ),
      appBar: new AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        iconTheme: new IconThemeData(color: Color(0XFF29D091)),
      ),
      body: Center(
        child: _pageOptions.elementAt(_bottomNavIndex),
    ),
    );
  }
}